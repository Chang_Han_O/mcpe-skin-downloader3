package com.funnythinks.mcpeskindownloader3;

import android.annotation.SuppressLint;
import android.app.*;
import android.content.*;
import android.net.*;
import android.os.*;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.*;
import android.widget.*;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.*;

import android.support.v4.app.Fragment;

@SuppressLint("ValidFragment")
public class others extends Fragment
{
	
	private Context mContext;

    public others(Context context) {
        mContext = context;
    }


	public void DownFile(String filename){
		File new_file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/MCPESKIN/");

		if(!new_file.exists()){
			new_file.mkdirs();
		}
		try {
			InputStream myInput = mContext.getAssets().open(filename);
			String outFileName_ = Environment.getExternalStorageDirectory() + "/MCPESKIN/" + filename;

			OutputStream myOutput_ = new FileOutputStream(outFileName_);
			//  File afile = new File(outFileName_);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = myInput.read(buffer)) > 0) {
				myOutput_.write(buffer, 0, length);
			}
			myOutput_.flush();
			myOutput_.close();
			myInput.close();
			Toast.makeText(mContext,getString(R.string.path_download) + outFileName_,Toast.LENGTH_LONG).show();
		}
		catch(IOException e) {
			e.printStackTrace();
			Toast.makeText(mContext,e.toString(),Toast.LENGTH_SHORT).show();
			Toast.makeText(mContext,"ERROR OCCURRED",Toast.LENGTH_SHORT).show();
		}
	}

	public void Apply(String filename){
		File new_file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/games/com.mojang/minecraftpe/");

		if(!new_file.exists()){
			new_file.mkdirs();
		}
		try {
			InputStream myInput = mContext.getAssets().open(filename);
			String outFileName_ = Environment.getExternalStorageDirectory() + "/games/com.mojang/minecraftpe/" + "custom.png";

			OutputStream myOutput_ = new FileOutputStream(outFileName_);
			//  File afile = new File(outFileName_);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = myInput.read(buffer)) > 0) {
				myOutput_.write(buffer, 0, length);
			}
			myOutput_.flush();
			myOutput_.close();
			myInput.close();

			new MaterialDialog.Builder(getActivity())
					.title(getString(R.string.success))
					.content(getString(R.string.skin_set))
					.positiveText(R.string.close) //Close String
					.titleColor(getResources().getColor(R.color.dialog))
					.positiveColor(getResources().getColor(R.color.dialog))
					.callback(new MaterialDialog.ButtonCallback() {
						@Override
						public void onPositive(MaterialDialog dialog) {

							dialog.dismiss();
						}
					}).show();
			Toast.makeText(mContext,getString(R.string.success_skin),Toast.LENGTH_SHORT).show();

		}
		catch(IOException e) {
			e.printStackTrace();
			Toast.makeText(mContext,e.toString(),Toast.LENGTH_SHORT).show();
			Toast.makeText(mContext,"ERROR OCCURRED",Toast.LENGTH_SHORT).show();
		}
	}




	public void MaterialDialog(String title, int lay, final String str) {

		new MaterialDialog.Builder(getActivity())
				.title(title)
				.customView(lay, true)
				.positiveText(R.string.close) //Close String
				.titleColor(getResources().getColor(R.color.dialog))
				.positiveColor(getResources().getColor(R.color.dialog))
				.negativeColor(getResources().getColor(R.color.dialog))
				.neutralColor(getResources().getColor(R.color.dialog))
				.negativeText(R.string.applys) //Apply Skin String
				.neutralText(R.string.downskin) //Download String
				.callback(new MaterialDialog.ButtonCallback() {
					@Override
					public void onPositive(MaterialDialog dialog) {

						dialog.dismiss();
					}

					@Override
					public void onNegative(MaterialDialog dialog) {

						Apply(str);

					}

					@Override
					public void onNeutral(MaterialDialog dialog) {
						DownFile(str);
					}
				}).show();

	}

	public void easySkin(int string, int layout, String name) {
		MaterialDialog(getString(string), layout, name);
	}
	
	ConnectivityManager connMgr;
	private static final String AD_UNIT_ID = "ca-app-pub-1643422267007813/1212287483";
	public final int DIALOG_DOWNLOAD_PROGRESS = 0;
	public ProgressDialog mProgressDialog;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.boy, null);
		connMgr = (ConnectivityManager)
			mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

		RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.list);

		ItemData data[] = {

				new ItemData(getString(R.string.ice_wizard), R.drawable.ice_wizardb),

				new ItemData(getString(R.string.mountain_path), R.drawable.mountain_pathb),

				new ItemData(getString(R.string.popcorn_surprise), R.drawable.popcorn_surpriseb),

				new ItemData(getString(R.string.donut), R.drawable.donutb),
				new ItemData(getString(R.string.yunus), R.drawable.yunusb),
				new ItemData(getString(R.string.bsw), R.drawable.bluespacewarriorb),
				new ItemData(getString(R.string.rsw), R.drawable.redspacewarriorb),
				new ItemData(getString(R.string.adidas), R.drawable.adidasb),
				new ItemData(getString(R.string.bacon), R.drawable.baconb),
				new ItemData(getString(R.string.troll), R.drawable.trollb),
				new ItemData(getString(R.string.firemage), R.drawable.firemageb),
				new ItemData(getString(R.string.astronaut), R.drawable.astronautb),
				new ItemData(getString(R.string.day), R.drawable.dayandnightb),
				new ItemData(getString(R.string.ghostgamer), R.drawable.ghostgamerb),
				new ItemData(getString(R.string.cyborg), R.drawable.cyborgb),
				new ItemData(getString(R.string.lava), R.drawable.lavab),
				new ItemData(getString(R.string.world), R.drawable.worldb),
				new ItemData(getString(R.string.ghost), R.drawable.ghostb),
				new ItemData(getString(R.string.magician), R.drawable.magicianb),
				new ItemData(getString(R.string.ilovereaha), R.drawable.ilovereahab)
		};

		//set layoutManager
		recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
		// 3. create an adapter
		MyAdapter mAdapter = new MyAdapter(data);
		// 4. set adapter
		recyclerView.setAdapter(mAdapter);
		// 5. set item animator to DefaultAnimator
		recyclerView.setItemAnimator(new DefaultItemAnimator());

		mAdapter.SetOnItemClickListener(new MyAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(View v, int position) {
				switch (position) {
					case 0:
						easySkin(R.string.ice_wizard, R.layout.ice_wizard, "ice_wizard.png");
						break;
					case 1:
						easySkin(R.string.mountain_path, R.layout.mountain_path, "mountain_path.png");
						break;
					case 2:
						easySkin(R.string.popcorn_surprise, R.layout.popcorn_surprise, "popcorn_surprise.png");
						break;
					case 3:
						easySkin(R.string.donut, R.layout.donut, "donut.png");
						break;
					case 4:
						easySkin(R.string.yunus, R.layout.yunus,"yunus.png");
						break;
					case 5:
						easySkin(R.string.bsw, R.layout.bsws,"blue_space_warrior.png");
						break;
					case 6:
						easySkin(R.string.rsw, R.layout.rsws,"red_space_warrior.png");
						break;
					case 7:
						easySkin(R.string.adidas, R.layout.adidass,"adidas.png");
						break;
					case 8:
						easySkin(R.string.bacon, R.layout.bacons,"bacon.png");
						break;
					case 9:
						easySkin(R.string.troll, R.layout.trolls,"troll.png");
						break;
					case 10:
						easySkin(R.string.firemage, R.layout.firemages,"fire_mage.png");
						break;
					case 11:
						easySkin(R.string.astronaut, R.layout.astronauts,"astronaut.png");
						break;
					case 12:
						easySkin(R.string.day, R.layout.days,"day_and_night.png");
						break;
					case 13:
						easySkin(R.string.ghostgamer, R.layout.ggs,"ghost_gamer.png");
						break;
					case 14:
						easySkin(R.string.cyborg, R.layout.cyborgs,"cyborg.png");
						break;
					case 15:
						easySkin(R.string.lava, R.layout.lavas,"lava_vs_water.png");
						break;
					case 16:
						easySkin(R.string.world, R.layout.worlds,"world.png");
						break;
					case 17:
						easySkin(R.string.ghost, R.layout.ghost,"ghost.png");
						break;
					case 18:
						easySkin(R.string.magician, R.layout.magician,"magician.png");
						break;
					case 19:
						easySkin(R.string.ilovereaha, R.layout.ilovereaha,"i_love_reaha.png");
						break;
				}

			}});

			return view;
			}

	/** Called before the activity is destroyed. */
	@Override
	public void onDestroy() {
		// Destroy the AdView.
		super.onDestroy();
	}
}
