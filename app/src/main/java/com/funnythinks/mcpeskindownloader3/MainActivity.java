package com.funnythinks.mcpeskindownloader3;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import static android.view.Gravity.START;
import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {

    private ViewGroup rowContainer, rowContainer2;
    private View view, view2;
    private AdView adView;
    private Toolbar toolbar;
    private float offset;
    private boolean flipped;
    private DrawerLayout drawer;
    private InterstitialAd interstitial;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private DrawerArrowDrawable drawerArrowDrawable;
    private static final String AD_UNIT_ID = "ca-app-pub-1643422267007813/1351888287";
    int i = 1;



    public void fillRow(View view, final String title, final String description) {

        TextView textView1 = (TextView) view.findViewById(R.id.textview);
        TextView textView2 = (TextView) view.findViewById(R.id.textview2);
        textView1.setText(title);
        textView2.setText(description);

        findViewById(R.id.card2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), blocksave.class));
            }
        });
        findViewById(R.id.card3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), site.class));
            }
        });
        findViewById(R.id.card4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MaterialDialog.Builder(MainActivity.this)
                        .title(R.string.devel)
                        .content(R.string.copyright)
                        .positiveText(R.string.close)
                        .titleColor(getResources().getColor(R.color.dialog))
                        .positiveColor(getResources().getColor(R.color.dialog))
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                dialog.dismiss();
                            }
                        }).show();
            }
        });
        findViewById(R.id.card5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), email.class));
            }
        });
        findViewById(R.id.card6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),apps.class));
            }
        });

    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);



        AdView adView = (AdView) findViewById(R.id.adView);

        interstitial = new InterstitialAd(MainActivity.this);
        interstitial.setAdUnitId(AD_UNIT_ID);
        interstitial.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
                startActivity(new Intent(MainActivity.this,skins.class));
            }
        });

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        // Begin loading your interstitial.
        interstitial.loadAd(adRequest);


        ListView mListView = (ListView) findViewById(R.id.drawer_list);
        ListView mListView2 = (ListView) findViewById(R.id.drawer_list2);

        final ArrayList<Listviewitem> data = new ArrayList<>();
        final ArrayList<Listviewitem> data2 = new ArrayList<>();
        final Listviewitem a = new Listviewitem(R.drawable.ic_google_play, getString(R.string.rate));
        final Listviewitem b = new Listviewitem(R.drawable.ic_information, getString(R.string.change_log_title));
        final Listviewitem c = new Listviewitem(R.drawable.ic_share_variant, getString(R.string.share));
        final Listviewitem d = new Listviewitem(R.drawable.ic_message_processing, getString(R.string.open_source));
        final Listviewitem e_ = new Listviewitem(R.drawable.ic_email_outline, getString(R.string.sending));
        final Listviewitem f = new Listviewitem(R.drawable.ic_close, getString(R.string.close_drawer));

        data.add(a);
        data.add(b);
        data.add(c);

        data2.add(d);
        data2.add(e_);
        data2.add(f);

        ListviewAdapter adapter = new ListviewAdapter(this, R.layout.widget_icontext, data);
        ListviewAdapter adapter2 = new ListviewAdapter(this, R.layout.widget_icontext, data2);
        mListView.setAdapter(adapter);
        mListView2.setAdapter(adapter2);

        final Resources resources = getResources();
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_main);
        drawer.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);

        drawerArrowDrawable = new DrawerArrowDrawable(resources);

        drawerArrowDrawable.setStrokeColor(resources.getColor(R.color.white));

        toolbar = (Toolbar) findViewById(R.id.toolbar_);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(drawerArrowDrawable);
        toolbar.setTitleTextColor(Color.WHITE);



        drawer.setDrawerListener(new DrawerLayout.SimpleDrawerListener()

                                 {
                                     @Override
                                     public void onDrawerSlide(View drawerView, float slideOffset) {
                                         offset = slideOffset;

                                         // Sometimes slideOffset ends up so close to but not quite 1 or 0.
                                         if (slideOffset >= .995) {
                                             flipped = true;
                                             drawerArrowDrawable.setFlip(flipped);
                                         } else if (slideOffset <= .005) {
                                             flipped = false;
                                             drawerArrowDrawable.setFlip(flipped);
                                         }

                                         drawerArrowDrawable.setParameter(offset);
                                     }
                                 }

        );

        toolbar.setNavigationOnClickListener(new View.OnClickListener()

                                             {
                                                 @Override
                                                 public void onClick(View v) {
                                                     if (drawer.isDrawerVisible(START)) {
                                                         drawer.closeDrawer(START);
                                                         return;
                                                     } else {
                                                         drawer.openDrawer(START);
                                                     }
                                                 }
                                             }

        );

        CardView cardView = (CardView)findViewById(R.id.card1);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (interstitial.isLoaded()) {
                    if (i == 1) {
                        interstitial.show();
                        i++;
                    } else {
                        startActivity(new Intent(MainActivity.this,skins.class));
                    }
                } else {
                    startActivity(new Intent(MainActivity.this,skins.class));
                }
            }
        });

        rowContainer = (LinearLayout) findViewById(R.id.rowContainer);
        rowContainer2 = (LinearLayout) findViewById(R.id.rowContainer2);


        view = rowContainer.findViewById(R.id.card2);
        fillRow(view, getString(R.string.how), getString(R.string.tooeasy));
        ((ImageView) view.findViewById(R.id.imageview)).setImageResource(R.drawable.apply);

        view = rowContainer.findViewById(R.id.card3);
        fillRow(view, getString(R.string.sitet), getString(R.string.site_main_text));
        ((ImageView) view.findViewById(R.id.imageview)).setImageResource(R.drawable.site);

        view2 = rowContainer2.findViewById(R.id.card4);
        fillRow(view2, getString(R.string.devel), "Funny Thinks");
        ((ImageView) view2.findViewById(R.id.imageview)).setImageResource(R.drawable.developer);

        view2 = rowContainer2.findViewById(R.id.card5);
        fillRow(view2, getString(R.string.report), getString(R.string.report_about_msd));
        ((ImageView) view2.findViewById(R.id.imageview)).setImageResource(R.drawable.report);




        String version;
        try

        {
            PackageInfo i = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = i.versionName;
        } catch (Exception e)

        {
            version = "";
        }

        SharedPreferences pref = getSharedPreferences("pref", Context.MODE_PRIVATE); // UI 상태를 저장합니다.
        final SharedPreferences.Editor editor = pref.edit(); // Editor를 불러옵니다
        editor.putString("check_version", version); // 저장할 값들을 입력 합니다.
        editor.commit(); // 저장합니다.

        String check_version = pref.getString("check_version", "");//
        String check_status = pref.getString("check_status", "");
        if (!check_version.equals(check_status))

        {
            new MaterialDialog.Builder(MainActivity.this)
                    .title(R.string.nt)
                    .content(R.string.nm)
                    .titleColor(getResources().getColor(R.color.dialog))
                    .positiveText(R.string.close)
                    .positiveColor(getResources().getColor(R.color.dialog))
                    .negativeColor(getResources().getColor(R.color.dialog))
                    .negativeText(R.string.never)
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {

                            dialog.dismiss();
                        }

                        @Override
                        public void onNegative(MaterialDialog dialog) {
                            String version;
                            try {
                                PackageInfo i = getPackageManager()
                                        .getPackageInfo(getPackageName(), 0);
                                version = i.versionName;
                            } catch (Exception e) {
                                version = "";
                            }
                            SharedPreferences pref =
                                    getSharedPreferences("pref", Context.MODE_PRIVATE); // UI 상태를 저장합니다.
                            SharedPreferences.Editor editor = pref.edit(); // Editor를 불러옵니다
                            editor.putString("check_status", version);
                            editor.commit(); // 저장합니다.
                            dialog.cancel();
                        }
                    }).show();


        }


        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                data.get(position);
                if (data.get(position) == (a)) {
                    drawer.closeDrawer(START);
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("market://details?id=com.funnythinks.mcpeskindownloader3"));
                    startActivity(intent);
                }
                if (data.get(position) == (b)) {
                    drawer.closeDrawer(START);
                    new MaterialDialog.Builder(MainActivity.this)
                            .title(R.string.change_log_title)
                            .content(R.string.change_log)
                            .titleColor(getResources().getColor(R.color.dialog))
                            .positiveText(R.string.close)
                            .positiveColor(getResources().getColor(R.color.dialog))
                            .callback(new MaterialDialog.ButtonCallback() {
                                @Override
                                public void onPositive(MaterialDialog dialog) {
                                    dialog.dismiss();
                                }
                            }).show();
                }
                if (data.get(position) == (c)) {
                    drawer.closeDrawer(START);
                    Intent intent1 = new Intent(Intent.ACTION_SEND);
                    intent1.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.mcpeskindownloader3));
                    intent1.setType("text/plain");
                    intent1.putExtra(Intent.EXTRA_TEXT, getString(R.string.km));
                    startActivity(Intent.createChooser(intent1, getString(R.string.share)));
                }
            }
        });



        mListView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                data2.get(position);
                if (data2.get(position) == (e_)) {
                    drawer.closeDrawer(START);
                    startActivity(new Intent(MainActivity.this, email.class));
                }
                data2.get(position);
                if (data2.get(position) == (f)) {
                    drawer.closeDrawer(START);
                }
                if (data2.get(position) == (d)) {
                    drawer.closeDrawer(START);
                    MaterialDialog dialog = new MaterialDialog.Builder(MainActivity.this)
                            .title(R.string.open_source)
                            .customView(R.layout.webview, true)
                            .positiveText(R.string.close)
                            .titleColor(getResources().getColor(R.color.dialog))
                            .positiveColor(getResources().getColor(R.color.dialog))
                            .build();
                    WebView webView = (WebView) dialog.getCustomView().findViewById(R.id.tabwebview);
                    webView.loadUrl("file:///android_asset/tab.html");
                    WebView webView2 = (WebView) dialog.getCustomView().findViewById(R.id.ripplewebview);
                    webView2.loadUrl("https://raw.githubusercontent.com/navasmdc/MaterialDesignLibrary/master/LICENSE");
                    WebView webView3 = (WebView) dialog.getCustomView().findViewById(R.id.material_dialog_webview);
                    webView3.loadUrl("https://raw.githubusercontent.com/afollestad/material-dialogs/master/LICENSE.txt");
                    WebView webView4 = (WebView) dialog.getCustomView().findViewById(R.id.drawable_webview);
                    webView4.loadUrl("https://raw.githubusercontent.com/ChrisRenke/DrawerArrowDrawable/master/LICENSE.txt");
                    dialog.show();
                }
            }
        });
    }

    public void displayInterstitial() {
        if (interstitial.isLoaded()) {
            interstitial.show();
        }
    }

    public void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder().build();
        // Begin loading your interstitial.
        interstitial.loadAd(adRequest);
    }


    @Override
    public void onBackPressed() {

            new MaterialDialog.Builder(this)
                    .content(R.string.exit)
                    .positiveText(R.string.n)
                    .positiveColor(getResources().getColor(R.color.dialog))
                    .negativeColor(getResources().getColor(R.color.dialog))
                    .negativeText(R.string.y)
                    .callback(new MaterialDialog.ButtonCallback() {
                                  @Override
                                  public void onPositive(MaterialDialog dialog) {

                                      dialog.dismiss();
                                  }

                                  @Override
                                  public void onNegative(MaterialDialog dialog) {
                                      System.exit(0);

                                  }
                              }
                    )
                    .show();
        }
    }