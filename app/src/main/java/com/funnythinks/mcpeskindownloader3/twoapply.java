package com.funnythinks.mcpeskindownloader3;

import android.app.*;
import android.content.*;
import android.os.*;
import android.view.*;
import android.widget.*;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class twoapply extends Activity {
	
	private AdView adView;
	private static final String AD_UNIT_ID = "ca-app-pub-1643422267007813/4600836685";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Window win = getWindow();
		win.requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.twoapply);
		
		adView = new AdView(this);
		adView.setAdSize(AdSize.SMART_BANNER);
		adView.setAdUnitId(AD_UNIT_ID);

		RelativeLayout layout = (RelativeLayout) findViewById(R.id.adlayout);
		layout.addView(adView);

		AdRequest adRequest = new AdRequest.Builder().build();
		adView.loadAd(adRequest);
		
		}
		
		public void a(View v){
			AlertDialog.Builder Dialog = new AlertDialog.Builder(this);
			Dialog.setTitle(R.string.easy);
			Dialog.setCancelable(true);
			Dialog.setMessage(R.string.easymessage);
			Dialog.setNegativeButton(R.string.close, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{

					}
				});
			Dialog.show(); 
		} 
		
		public void b(View v){
			startActivity(new Intent(this,blocksave.class));
			
		}
		
}
