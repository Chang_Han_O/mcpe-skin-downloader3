package com.funnythinks.mcpeskindownloader3;

import android.content.*;
import android.graphics.Color;
import android.net.*;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.*;
import android.widget.*;
import android.view.View.OnClickListener;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import android.os.Bundle;

public class site extends ActionBarActivity {

	private ViewGroup rowContainer;
	private View view;
    private AdView adView;
    private static final String AD_UNIT_ID = "ca-app-pub-1643422267007813/1212287483";

	public void fillRow(View view, final String title, final String description) {

		TextView textView1 = (TextView) view.findViewById(R.id.textview_site);
		TextView textView2 = (TextView) view.findViewById(R.id.textview2_site);
		textView1.setText(title);
		textView2.setText(description);

		findViewById(R.id.card1_site).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("http://www.planetminecraft.com/resources/skins"));
				startActivity(intent);
			}
		});
		findViewById(R.id.card2_site).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("http://www.minecraftskins.com/"));
				startActivity(intent);
			}
		});
		findViewById(R.id.card3_site).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("http://www.minecraftskins.net/"));
				startActivity(intent);
			}
		});
		findViewById(R.id.card4_site).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("http://skincache.com/"));
				startActivity(intent);
			}
		});

	}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.site);

		Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(mToolbar);
		mToolbar.setTitleTextColor(Color.WHITE);

		ActionBar mActionBar = getSupportActionBar();

		if (mActionBar != null) {
			mActionBar.setHomeButtonEnabled(true);
			mActionBar.setDisplayHomeAsUpEnabled(true);

			mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					onBackPressed();
				}
			});
		}

        adView = new AdView(this);
	adView.setAdSize(AdSize.SMART_BANNER);
	adView.setAdUnitId(AD_UNIT_ID);

	RelativeLayout layout = (RelativeLayout) findViewById(R.id.adlayout);
	layout.addView(adView);

	AdRequest adRequest = new AdRequest.Builder()
		.build();

	adView.loadAd(adRequest);

		rowContainer = (LinearLayout) findViewById(R.id.rowContainer_site);

		view = rowContainer.findViewById(R.id.card1_site);
		fillRow(view, "PLANET MINECRAFT", "http://www.planetminecraft.com/resources/skins");

		view = rowContainer.findViewById(R.id.card2_site);
		fillRow(view, "The Skindex","http://www.minecraftskins.com/");

		view = rowContainer.findViewById(R.id.card3_site);
		fillRow(view, "MinecraftSkins.net","http://www.minecraftskins.net/");

		view = rowContainer.findViewById(R.id.card4_site);
		fillRow(view, "SKINCACHE","http://skincache.com/");
		
		}
	public void onResume() {
		super.onResume();
		if (adView != null) {
			adView.resume();
		}
	}

	@Override
	public void onPause() {
		if (adView != null) {
			adView.pause();
		}
		super.onPause();
	}

	/** Called before the activity is destroyed. */
	@Override
	public void onDestroy() {
		// Destroy the AdView.
		if (adView != null) {
			adView.destroy();
		}
		super.onDestroy();
	}
}
		
