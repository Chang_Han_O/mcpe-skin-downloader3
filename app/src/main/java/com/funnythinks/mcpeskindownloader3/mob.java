package com.funnythinks.mcpeskindownloader3;

import android.annotation.SuppressLint;
import android.app.*;
import android.content.*;
import android.net.*;
import android.os.*;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.*;
import android.widget.*;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import java.io.*;
import java.util.*;

import android.support.v4.app.Fragment;

@SuppressLint("ValidFragment")
public class mob extends Fragment
{
	private Context mContext;

    public mob(Context context) {
        mContext = context;
    }

	public void DownFile(String filename){
		File new_file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/MCPESKIN/");

		if(!new_file.exists()){
			new_file.mkdirs();
		}
		try {
			InputStream myInput = mContext.getAssets().open(filename);
			String outFileName_ = Environment.getExternalStorageDirectory() + "/MCPESKIN/" + filename;

			OutputStream myOutput_ = new FileOutputStream(outFileName_);
			//  File afile = new File(outFileName_);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = myInput.read(buffer)) > 0) {
				myOutput_.write(buffer, 0, length);
			}
			myOutput_.flush();
			myOutput_.close();
			myInput.close();
			Toast.makeText(mContext,getString(R.string.path_download) + outFileName_,Toast.LENGTH_LONG).show();
		}
		catch(IOException e) {
			e.printStackTrace();
			Toast.makeText(mContext,e.toString(),Toast.LENGTH_SHORT).show();
			Toast.makeText(mContext,"ERROR OCCURRED",Toast.LENGTH_SHORT).show();
		}
	}

	public void Apply(String filename){
		File new_file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/games/com.mojang/minecraftpe/");

		if(!new_file.exists()){
			new_file.mkdirs();
		}
		try {
			InputStream myInput = mContext.getAssets().open(filename);
			String outFileName_ = Environment.getExternalStorageDirectory() + "/games/com.mojang/minecraftpe/" + "custom.png";

			OutputStream myOutput_ = new FileOutputStream(outFileName_);
			//  File afile = new File(outFileName_);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = myInput.read(buffer)) > 0) {
				myOutput_.write(buffer, 0, length);
			}
			myOutput_.flush();
			myOutput_.close();
			myInput.close();

			new MaterialDialog.Builder(getActivity())
					.title(getString(R.string.success))
					.content(getString(R.string.skin_set))
					.positiveText(R.string.close) //Close String
					.titleColor(getResources().getColor(R.color.dialog))
					.positiveColor(getResources().getColor(R.color.dialog))
					.callback(new MaterialDialog.ButtonCallback() {
						@Override
						public void onPositive(MaterialDialog dialog) {

							dialog.dismiss();
						}
					}).show();
			Toast.makeText(mContext,getString(R.string.success_skin),Toast.LENGTH_SHORT).show();

		}
		catch(IOException e) {
			e.printStackTrace();
			Toast.makeText(mContext,e.toString(),Toast.LENGTH_SHORT).show();
			Toast.makeText(mContext,"ERROR OCCURRED",Toast.LENGTH_SHORT).show();
		}
	}




	public void MaterialDialog(String title, int lay, final String str) {

		new MaterialDialog.Builder(getActivity())
				.title(title)
				.customView(lay, true)
				.positiveText(R.string.close) //Close String
				.titleColor(getResources().getColor(R.color.dialog))
				.positiveColor(getResources().getColor(R.color.dialog))
				.negativeColor(getResources().getColor(R.color.dialog))
				.neutralColor(getResources().getColor(R.color.dialog))
				.negativeText(R.string.applys) //Apply Skin String
				.neutralText(R.string.downskin) //Download String
				.callback(new MaterialDialog.ButtonCallback() {
					@Override
					public void onPositive(MaterialDialog dialog) {

						dialog.dismiss();
					}

					@Override
					public void onNegative(MaterialDialog dialog) {

						Apply(str);

					}

					@Override
					public void onNeutral(MaterialDialog dialog) {
						DownFile(str);
					}
				}).show();

	}

	public void easySkin(int string, int layout, String name) {
		MaterialDialog(getString(string), layout, name);
	}
	
	ConnectivityManager connMgr;
	private AdView adView;
	private static final String AD_UNIT_ID = "ca-app-pub-1643422267007813/1212287483";
	public final int DIALOG_DOWNLOAD_PROGRESS = 0;
	public ProgressDialog mProgressDialog;
	ArrayList<String> data;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.boy, null);
		connMgr = (ConnectivityManager)
			mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		adView = new AdView(getActivity());
		adView.setAdSize(AdSize.SMART_BANNER);
		adView.setAdUnitId(AD_UNIT_ID);

		RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.list);

		ItemData data[] = {
				new ItemData(getString(R.string.diamond_golem), R.drawable.diamond_golemb),
				new ItemData(getString(R.string.fire_ice_creeper), R.drawable.fire_ice_creeperb),
				new ItemData(getString(R.string.iron_creeper), R.drawable.iron_creeperb),
				new ItemData(getString(R.string.villager_nomal), R.drawable.villagerb),
				new ItemData(getString(R.string.mobspawner), R.drawable.mobspawnerb),
				new ItemData(getString(R.string.villager), R.drawable.villagerinunderwearb),
				new ItemData(getString(R.string.fusionmob), R.drawable.fusionmobb),
				new ItemData(getString(R.string.goldenzombie), R.drawable.goldenzombieb),
				new ItemData(getString(R.string.slimeman), R.drawable.slimemanb),
				new ItemData(getString(R.string.colorful), R.drawable.colorfulcreeperb),
				new ItemData(getString(R.string.gc), R.drawable.goldencreeperb),
				new ItemData(getString(R.string.skeleton), R.drawable.skeletonb),
				new ItemData(getString(R.string.tc), R.drawable.troncreeperb),
				new ItemData(getString(R.string.creepersanta), R.drawable.creepersantab),
				new ItemData(getString(R.string.dave), R.drawable.daveb),
				new ItemData(getString(R.string.irongolem), R.drawable.golemb),
				new ItemData(getString(R.string.endergreen), R.drawable.endergreenb),
				new ItemData(getString(R.string.ne), R.drawable.neonb),
				new ItemData(getString(R.string.hb), R.drawable.herobrineb),
				new ItemData(getString(R.string.dj), R.drawable.djendermanb),
				new ItemData(getString(R.string.creeper), R.drawable.creepersuitb),
				new ItemData(getString(R.string.mask), R.drawable.creepermaskb),
				new ItemData(getString(R.string.enderhuman), R.drawable.enderhumanb),
				new ItemData(getString(R.string.hood), R.drawable.enderhoodb)
		};

		//set layoutManager
		recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
		// 3. create an adapter
		MyAdapter mAdapter = new MyAdapter(data);
		// 4. set adapter
		recyclerView.setAdapter(mAdapter);
		// 5. set item animator to DefaultAnimator
		recyclerView.setItemAnimator(new DefaultItemAnimator());

		mAdapter.SetOnItemClickListener(new MyAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(View v, int position) {
				switch (position) {
					case 0:
						easySkin(R.string.diamond_golem, R.layout.diamond_golems, "diamond_golem.png");
						break;
					case 1:
						easySkin(R.string.fire_ice_creeper, R.layout.fire_ice_creeper, "fire_ice_creeper.png");
						break;
					case 2:
						easySkin(R.string.iron_creeper, R.layout.iron_creepers, "iron_creeper.png");
						break;
					case 3:
						easySkin(R.string.villager_nomal, R.layout.villager_nomals,"villager.png");
						break;
					case 4:
						easySkin(R.string.mobspawner, R.layout.mobspawners,"mob_spawner.png");
						break;
					case 5:
						easySkin(R.string.villager, R.layout.villagers,"villager_in_underwear.png");
						break;
					case 6:
						easySkin(R.string.fusionmob, R.layout.fusionmobs,"fusion_mob.png");
						break;
					case 7:
						easySkin(R.string.goldenzombie, R.layout.goldenzombies,"golden_zombie.png");
						break;
					case 8:
						easySkin(R.string.slimeman, R.layout.slimemans,"slime_man.png");
						break;
					case 9:
						easySkin(R.string.colorful, R.layout.colorfuls,"colorful_creeper.png");
						break;
					case 10:
						easySkin(R.string.gc, R.layout.gcs,"golden_creeper.png");
						break;
					case 11:
						easySkin(R.string.skeleton, R.layout.skeletons,"skeleton.png");
						break;
					case 12:
						easySkin(R.string.tc, R.layout.tcs,"tron_creeper.png");
						break;
					case 13:
						easySkin(R.string.creepersanta, R.layout.creepersantas,"creeper_santa.png");
						break;
					case 14:
						easySkin(R.string.dave, R.layout.daves,"dave.png");
						break;
					case 15:
						easySkin(R.string.irongolem, R.layout.irongolems,"golem.png");
						break;
					case 16:
						easySkin(R.string.endergreen, R.layout.endergreens,"ender_green.png");
						break;
					case 17:
						easySkin(R.string.ne, R.layout.nes,"neon_enderman.png");
						break;
					case 18:
						easySkin(R.string.hb, R.layout.herobrine,"herobrine.png");
						break;
					case 19:
						easySkin(R.string.dj, R.layout.dj,"dj_enderman.png");
						break;
					case 20:
						easySkin(R.string.creeper, R.layout.creeper,"creeper_suit.png");
						break;
					case 21:
						easySkin(R.string.mask, R.layout.mask,"creeper_mask.png");
						break;
					case 22:
						easySkin(R.string.enderhuman, R.layout.enderhuman,"enderhuman.png");
						break;
					case 23:
						easySkin(R.string.hood, R.layout.hood,"hoodender.png");
						break;
				}

			}});
	return view;
}

	/** Called before the activity is destroyed. */
	@Override
	public void onDestroy() {
		// Destroy the AdView.
		super.onDestroy();
	}
}
