package com.funnythinks.mcpeskindownloader3;

import android.annotation.SuppressLint;
import android.app.*;
import android.content.*;
import android.net.*;
import android.os.*;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.*;
import android.widget.*;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.*;

import android.support.v4.app.Fragment;
@SuppressLint("ValidFragment")
public class animal extends Fragment
{

	private Context mContext;

   public animal(Context context) {
        mContext = context;
    }

    public void DownFile(String filename){
        File new_file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/MCPESKIN/");

        if(!new_file.exists()){
            new_file.mkdirs();
        }
        try {
            InputStream myInput = mContext.getAssets().open(filename);
            String outFileName_ = Environment.getExternalStorageDirectory() + "/MCPESKIN/" + filename;

            OutputStream myOutput_ = new FileOutputStream(outFileName_);
            //  File afile = new File(outFileName_);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput_.write(buffer, 0, length);
            }
            myOutput_.flush();
            myOutput_.close();
            myInput.close();
            Toast.makeText(mContext,getString(R.string.path_download) + outFileName_,Toast.LENGTH_LONG).show();
        }
        catch(IOException e) {
            e.printStackTrace();
            Toast.makeText(mContext,e.toString(),Toast.LENGTH_SHORT).show();
            Toast.makeText(mContext,"ERROR OCCURRED",Toast.LENGTH_SHORT).show();
        }
    }

    public void Apply(String filename){
        File new_file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/games/com.mojang/minecraftpe/");

        if(!new_file.exists()){
            new_file.mkdirs();
        }
        try {
            InputStream myInput = mContext.getAssets().open(filename);
            String outFileName_ = Environment.getExternalStorageDirectory() + "/games/com.mojang/minecraftpe/" + "custom.png";

            OutputStream myOutput_ = new FileOutputStream(outFileName_);
            //  File afile = new File(outFileName_);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput_.write(buffer, 0, length);
            }
            myOutput_.flush();
            myOutput_.close();
            myInput.close();

            new MaterialDialog.Builder(getActivity())
                    .title(getString(R.string.success))
                    .content(getString(R.string.skin_set))
                    .positiveText(R.string.close) //Close String
                    .titleColor(getResources().getColor(R.color.dialog))
                    .positiveColor(getResources().getColor(R.color.dialog))
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {

                            dialog.dismiss();
                        }
                    }).show();
            Toast.makeText(mContext,getString(R.string.success_skin),Toast.LENGTH_SHORT).show();

        }
        catch(IOException e) {
            e.printStackTrace();
            Toast.makeText(mContext,e.toString(),Toast.LENGTH_SHORT).show();
            Toast.makeText(mContext,"ERROR OCCURRED",Toast.LENGTH_SHORT).show();
        }
    }




    public void MaterialDialog(String title, int lay, final String str) {

        new MaterialDialog.Builder(getActivity())
                .title(title)
                .customView(lay, true)
                .positiveText(R.string.close) //Close String
                .titleColor(getResources().getColor(R.color.dialog))
                .positiveColor(getResources().getColor(R.color.dialog))
                .negativeColor(getResources().getColor(R.color.dialog))
                .neutralColor(getResources().getColor(R.color.dialog))
                .negativeText(R.string.applys) //Apply Skin String
                .neutralText(R.string.downskin) //Download String
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                        dialog.dismiss();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {

                        Apply(str);

                    }

                    @Override
                    public void onNeutral(MaterialDialog dialog) {
                        DownFile(str);
                    }
                }).show();

    }



    public void easySkin(int string, int layout, String name) {
        MaterialDialog(getString(string), layout, name);
    }
	
	
	ConnectivityManager connMgr;
	private static final String AD_UNIT_ID = "ca-app-pub-1643422267007813/1212287483";
	public final int DIALOG_DOWNLOAD_PROGRESS = 0;
	public ProgressDialog mProgressDialog;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.boy, null);
		connMgr = (ConnectivityManager)
			mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.list);

        ItemData data[] = {
                new ItemData(getString(R.string.lion), R.drawable.lionb),
                new ItemData(getString(R.string.bear), R.drawable.bearb),
                new ItemData(getString(R.string.blue_dragon), R.drawable.blue_dragonb),
                new ItemData(getString(R.string.cheetah), R.drawable.cheetahb),
                new ItemData(getString(R.string.dog), R.drawable.dogb),
                new ItemData(getString(R.string.duck), R.drawable.duckb),
                new ItemData(getString(R.string.frog), R.drawable.frogb),
                new ItemData(getString(R.string.koala), R.drawable.koalab),
                new ItemData(getString(R.string.owl), R.drawable.owlb),
                new ItemData(getString(R.string.penguin), R.drawable.penguinb),
                new ItemData(getString(R.string.turtle), R.drawable.turtleb),
                new ItemData(getString(R.string.whale), R.drawable.whaleb),
                new ItemData(getString(R.string.iron_mooshroom), R.drawable.iron_mooshroomb),
                new ItemData(getString(R.string.siamesecat), R.drawable.siamesecatb),
                new ItemData(getString(R.string.angry), R.drawable.angryb),
                new ItemData(getString(R.string.wearwolf), R.drawable.wearwolfb),
                new ItemData(getString(R.string.cutepanda), R.drawable.cutepandab),
                new ItemData(getString(R.string.tiger), R.drawable.tigerb),
                new ItemData(getString(R.string.deadpig), R.drawable.deadpigb),
                new ItemData(getString(R.string.sheep), R.drawable.sheepb),
                new ItemData(getString(R.string.supersheep), R.drawable.supersheepb),
                new ItemData(getString(R.string.coolcow), R.drawable.coolcowb),
                new ItemData(getString(R.string.agentchicken), R.drawable.agentchickenb),
                new ItemData(getString(R.string.mushroom), R.drawable.mooshroomb),
                new ItemData(getString(R.string.beagle), R.drawable.beagleb),
                new ItemData(getString(R.string.squid), R.drawable.squidb),
                new ItemData(getString(R.string.dragon), R.drawable.dragonb),
                new ItemData(getString(R.string.doge), R.drawable.dogeb),
                new ItemData(getString(R.string.dp), R.drawable.diamondpigb)
        };

        //set layoutManager
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        // 3. create an adapter
        MyAdapter mAdapter = new MyAdapter(data);
        // 4. set adapter
        recyclerView.setAdapter(mAdapter);
        // 5. set item animator to DefaultAnimator
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        mAdapter.SetOnItemClickListener(new MyAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                switch (position) {
                    case 0:
                        easySkin(R.string.lion, R.layout.lion,"lion.png");
                        break;
                    case 1:
                        easySkin(R.string.bear, R.layout.bears,"bear.png");
                        break;
                    case 2:
                        easySkin(R.string.blue_dragon, R.layout.blue_dragons,"blue_dragon.png");
                        break;
                    case 3:
                        easySkin(R.string.cheetah, R.layout.cheetah,"cheetah.png");
                        break;
                    case 4:
                        easySkin(R.string.dog, R.layout.dog,"dog.png");
                        break;
                    case 5:
                        easySkin(R.string.duck, R.layout.duck,"duck.png");
                        break;
                    case 6:
                        easySkin(R.string.frog, R.layout.frog,"frog.png");
                        break;
                    case 7:
                        easySkin(R.string.koala, R.layout.koala,"koala.png");
                        break;
                    case 8:
                        easySkin(R.string.owl, R.layout.owl,"owl.png");
                        break;
                    case 9:
                        easySkin(R.string.penguin, R.layout.penguin,"penguin.png");
                        break;
                    case 10:
                        easySkin(R.string.turtle, R.layout.turtle,"turtle.png");
                        break;
                    case 11:
                        easySkin(R.string.whale, R.layout.whale,"whale.png");
                        break;
                    case 12:
                        easySkin(R.string.iron_mooshroom, R.layout.iron_mooshrooms,"iron_mooshroom.png");
                        break;
                    case 13:
                        easySkin(R.string.siamesecat, R.layout.siamesecats,"siamese_cat.png");
                        break;
                    case 14:
                        easySkin(R.string.angry, R.layout.angrys,"angry.png");
                        break;
                    case 15:
                        easySkin(R.string.wearwolf, R.layout.wearwolfs,"wearwolf.png");
                        break;
                    case 16:
                        easySkin(R.string.cutepanda, R.layout.cutepandas,"cute_panda.png");
                        break;
                    case 17:
                        easySkin(R.string.tiger, R.layout.tigers,"tiger.png");
                        break;
                    case 18:
                        easySkin(R.string.deadpig, R.layout.deadpigs,"dead_pig.png");
                        break;
                    case 19:
                        easySkin(R.string.sheep, R.layout.sheep,"sheep.png");
                        break;
                    case 20:
                        easySkin(R.string.supersheep, R.layout.supersheeps,"super_sheep.png");
                        break;
                    case 21:
                        easySkin(R.string.coolcow, R.layout.coolcows,"cool_cow.png");
                        break;
                    case 22:
                        easySkin(R.string.agentchicken, R.layout.agentchickens,"agent_chicken.png");
                        break;
                    case 23:
                        easySkin(R.string.mushroom, R.layout.mushrooms,"mooshroom.png");
                        break;
                    case 24:
                        easySkin(R.string.beagle, R.layout.beagles,"beagle.png");
                        break;
                    case 25:
                        easySkin(R.string.squid, R.layout.squids,"squid.png");
                        break;
                    case 26:
                        easySkin(R.string.dragon, R.layout.dragons,"dragon.png");
                        break;
                    case 27:
                        easySkin(R.string.doge, R.layout.doges,"doge.png");
                        break;
                    case 28:
                        easySkin(R.string.dp, R.layout.dps,"diamond_pig.png");
                        break;

                }
            }
				
			});return view;
			}

	/** Called before the activity is destroyed. */
	@Override
	public void onDestroy() {
		// Destroy the AdView.

		super.onDestroy();
	}
}
