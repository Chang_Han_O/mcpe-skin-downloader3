package com.funnythinks.mcpeskindownloader3;

public class ItemData {

    private String title;
    private int imageUrl;
    private String download, apply;

    public ItemData(String title,int imageUrl){

        this.title = title;
        this.imageUrl = imageUrl;

    }

    public String getTitle() {
        return title;
    }

    public String getDownload() {
        return download;
    }


    public int getImageUrl() {
        return imageUrl;
    }
    // getters & setters
}
