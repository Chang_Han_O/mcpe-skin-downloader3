package com.funnythinks.mcpeskindownloader3;

import android.graphics.Color;
import android.os.*;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.widget.*;
import android.content.*;
import android.view.View;
import android.widget.Toast;
import android.widget.EditText;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import android.support.v7.widget.Toolbar;

public class email extends AppCompatActivity {
    EditText edit;
	private AdView adView;
	private static final String AD_UNIT_ID = "ca-app-pub-1643422267007813/1212287483";
@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.email);

	Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
	setSupportActionBar(mToolbar);
	mToolbar.setTitleTextColor(Color.WHITE);

	ActionBar mActionBar = getSupportActionBar();

	if (mActionBar != null) {
		mActionBar.setHomeButtonEnabled(true);
		mActionBar.setDisplayHomeAsUpEnabled(true);

		mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onBackPressed();
			}
		});
	}

	adView = new AdView(this);
	adView.setAdSize(AdSize.SMART_BANNER);
	adView.setAdUnitId(AD_UNIT_ID);

	RelativeLayout layout = (RelativeLayout) findViewById(R.id.adlayout);
	layout.addView(adView);

	AdRequest adRequest = new AdRequest.Builder()
		.build();

	adView.loadAd(adRequest);

	edit = (EditText) findViewById(R.id.edit);

    findViewById(R.id.send_email).setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v){
            if(edit.length() > 0) {
            try{
                final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.setType("plain/text");
                intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"ochh508@naver.com"});
                intent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.et));
                String txt = edit.getText().toString();
                intent.putExtra(android.content.Intent.EXTRA_TEXT, "" + txt);
                startActivity(Intent.createChooser(intent, getString(R.string.sending)));

            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(email.this, getString(R.string.e1), Toast.LENGTH_SHORT).show();
            }

        }
            else {
                Toast.makeText(email.this, getString(R.string.email_length), Toast.LENGTH_LONG).show();
            }
    }});

}
	public void onResume() {
		super.onResume();
		if (adView != null) {
			adView.resume();
		}
	}

	@Override
	public void onPause() {
		if (adView != null) {
			adView.pause();
		}
		super.onPause();
	}

	/** Called before the activity is destroyed. */
	@Override
	public void onDestroy() {
		// Destroy the AdView.
		if (adView != null) {
			adView.destroy();
		}
		super.onDestroy();
	}
}
