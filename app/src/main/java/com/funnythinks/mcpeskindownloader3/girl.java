package com.funnythinks.mcpeskindownloader3;

import android.annotation.SuppressLint;
import android.content.*;
import android.net.*;
import android.os.*;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.*;
import android.widget.*;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.*;

import android.support.v4.app.Fragment;

@SuppressLint("ValidFragment")
public class girl extends Fragment
{
	private Context mContext;

    public girl(Context context) {
        mContext = context;
    }

    public void DownFile(String filename){
        File new_file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/MCPESKIN/");

        if(!new_file.exists()){
            new_file.mkdirs();
        }
        try {
            InputStream myInput = mContext.getAssets().open(filename);
            String outFileName_ = Environment.getExternalStorageDirectory() + "/MCPESKIN/" + filename;

            OutputStream myOutput_ = new FileOutputStream(outFileName_);
            //  File afile = new File(outFileName_);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput_.write(buffer, 0, length);
            }
            myOutput_.flush();
            myOutput_.close();
            myInput.close();
            Toast.makeText(mContext,getString(R.string.path_download) + outFileName_,Toast.LENGTH_LONG).show();
        }
        catch(IOException e) {
            e.printStackTrace();
            Toast.makeText(mContext,e.toString(),Toast.LENGTH_SHORT).show();
            Toast.makeText(mContext,"ERROR OCCURRED",Toast.LENGTH_SHORT).show();
        }
    }

    public void Apply(String filename){
        File new_file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/games/com.mojang/minecraftpe/");

        if(!new_file.exists()){
            new_file.mkdirs();
        }
        try {
            InputStream myInput = mContext.getAssets().open(filename);
            String outFileName_ = Environment.getExternalStorageDirectory() + "/games/com.mojang/minecraftpe/" + "custom.png";

            OutputStream myOutput_ = new FileOutputStream(outFileName_);
            //  File afile = new File(outFileName_);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput_.write(buffer, 0, length);
            }
            myOutput_.flush();
            myOutput_.close();
            myInput.close();

            new MaterialDialog.Builder(getActivity())
                    .title(getString(R.string.success))
                    .content(getString(R.string.skin_set))
                    .positiveText(R.string.close) //Close String
                    .titleColor(getResources().getColor(R.color.dialog))
                    .positiveColor(getResources().getColor(R.color.dialog))
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {

                            dialog.dismiss();
                        }
                    }).show();
            Toast.makeText(mContext,getString(R.string.success_skin),Toast.LENGTH_SHORT).show();

        }
        catch(IOException e) {
            e.printStackTrace();
            Toast.makeText(mContext,e.toString(),Toast.LENGTH_SHORT).show();
            Toast.makeText(mContext,"ERROR OCCURRED",Toast.LENGTH_SHORT).show();
        }
    }




    public void MaterialDialog(String title, int lay, final String str) {

        new MaterialDialog.Builder(getActivity())
                .title(title)
                .customView(lay, true)
                .positiveText(R.string.close) //Close String
                .titleColor(getResources().getColor(R.color.dialog))
                .positiveColor(getResources().getColor(R.color.dialog))
                .negativeColor(getResources().getColor(R.color.dialog))
                .neutralColor(getResources().getColor(R.color.dialog))
                .negativeText(R.string.applys) //Apply Skin String
                .neutralText(R.string.downskin) //Download String
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                        dialog.dismiss();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {

                        Apply(str);

                    }

                    @Override
                    public void onNeutral(MaterialDialog dialog) {
                        DownFile(str);
                    }
                }).show();

    }

    public void easySkin(int string, int layout, String name) {
        MaterialDialog(getString(string), layout, name);
    }

	
	ConnectivityManager connMgr;

	private static final String AD_UNIT_ID = "ca-app-pub-1643422267007813/1212287483";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.boy, null);
		connMgr = (ConnectivityManager)
			mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.list);

        ItemData data[] = {

                //v5.5 skin
                new ItemData(getString(R.string.wedding_dress), R.drawable.wedding_dressb),

                //v5.4 skin
                new ItemData(getString(R.string.ninja_girl), R.drawable.ninja_girlb),
                //v5.2 skin
                new ItemData(getString(R.string.love_snow), R.drawable.love_snowb),
                //v2.6 skin
                new ItemData(getString(R.string.dino_girl), R.drawable.dino_girlb),
                new ItemData(getString(R.string.diamond_queen), R.drawable.diamond_queenb),
                new ItemData(getString(R.string.fire_dragon), R.drawable.fire_dragonb),
                new ItemData(getString(R.string.ocean_queen), R.drawable.ocean_queenb),
                new ItemData(getString(R.string.white_sea_dress), R.drawable.white_sea_dressb),
                new ItemData(getString(R.string.easter), R.drawable.easterb),
                new ItemData(getString(R.string.fairy), R.drawable.fairyb),
                new ItemData(getString(R.string.music_girl), R.drawable.music_girlb),
                new ItemData(getString(R.string.spring_girl), R.drawable.spring_girlb),
                new ItemData(getString(R.string.tiger_girl), R.drawable.tiger_girlb),
                new ItemData(getString(R.string.sportsgirl), R.drawable.sportsgirlb),
                new ItemData(getString(R.string.cakegirl), R.drawable.cakegirlb),
                new ItemData(getString(R.string.thestar), R.drawable.thestarb),
                new ItemData(getString(R.string.hoodiegirl), R.drawable.hoodiegirlb),
                new ItemData(getString(R.string.summertime), R.drawable.summertimeb),
                new ItemData(getString(R.string.bcg), R.drawable.bluecreepergirlb),
                new ItemData(getString(R.string.summernight), R.drawable.summernightb),
                new ItemData(getString(R.string.panda), R.drawable.pinkpandagirlb),
                new ItemData(getString(R.string.withergirl), R.drawable.withergirlb),
                new ItemData(getString(R.string.galaxygirl), R.drawable.galaxygirlb),
                new ItemData(getString(R.string.blondegirl), R.drawable.blondegirlb),
                new ItemData(getString(R.string.cdg), R.drawable.cutedressgirlb),
                new ItemData(getString(R.string.candy), R.drawable.candypopb),
                new ItemData(getString(R.string.junglegirl), R.drawable.junglegirlb),
                new ItemData(getString(R.string.catgirl), R.drawable.catgirlb),
                new ItemData(getString(R.string.countrygirl), R.drawable.countrygirlb),
                new ItemData(getString(R.string.cutegirl), R.drawable.cutegirlb),
                new ItemData(getString(R.string.diamondgirl), R.drawable.diamondgirlb),
                new ItemData(getString(R.string.endergirl), R.drawable.endergirlb),
                new ItemData(getString(R.string.rbg), R.drawable.rainbowgirlb),
                new ItemData(getString(R.string.fg), R.drawable.flower_girlb),
                new ItemData(getString(R.string.assg), R.drawable.assasin_girlb)
        };

        //set layoutManager
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        // 3. create an adapter
        MyAdapter mAdapter = new MyAdapter(data);
        // 4. set adapter
        recyclerView.setAdapter(mAdapter);
        // 5. set item animator to DefaultAnimator
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        mAdapter.SetOnItemClickListener(new MyAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                switch (position) {
                    case 0:
                        easySkin(R.string.wedding_dress, R.layout.wedding_dress,"wedding_dress.png");
                        break;
                    case 1:
                        easySkin(R.string.ninja_girl, R.layout.ninja_girl,"ninja_girl.png");
                        break;
                    case 2:
                        easySkin(R.string.love_snow, R.layout.love_snow,"love_snow.png");
                        break;
                    case 3:
                        easySkin(R.string.dino_girl, R.layout.dino_girl,"dino_girl.png");
                        break;
                    case 4:
                        easySkin(R.string.diamond_queen, R.layout.diamond_queen,"diamond_queen.png");
                        break;
                    case 5:
                        easySkin(R.string.fire_dragon, R.layout.fire_dragon,"fire_dragon.png");
                        break;
                    case 6:
                        easySkin(R.string.ocean_queen, R.layout.ocean_queen,"ocean_queen.png");
                        break;
                    case 7:
                        easySkin(R.string.white_sea_dress, R.layout.white_sea_dress,"white_sea_dress.png");
                        break;
                    case 8:
                        easySkin(R.string.easter, R.layout.easters,"easter.png");
                        break;
                    case 9:
                        easySkin(R.string.fairy, R.layout.fairys,"fairy.png");
                        break;
                    case 10:
                        easySkin(R.string.music_girl, R.layout.music_girl,"music_girl.png");
                        break;
                    case 11:
                        easySkin(R.string.spring_girl, R.layout.spring_girl,"spring_girl.png");
                        break;
                    case 12:
                        easySkin(R.string.tiger_girl, R.layout.tiger_girls,"tiger_girl.png");
                        break;
                    case 13:
                        easySkin(R.string.sportsgirl, R.layout.sportgirls,"sports_girl.png");
                        break;
                    case 14:
                        easySkin(R.string.cakegirl, R.layout.cakegirls,"cake_girl.png");
                        break;
                    case 15:
                        easySkin(R.string.thestar, R.layout.thestars,"the_star.png");
                        break;
                    case 16:
                        easySkin(R.string.hoodiegirl, R.layout.hoodiegirls,"hoodie_girl.png");
                        break;
                    case 17:
                        easySkin(R.string.summertime, R.layout.summertimes,"summer_time.png");
                        break;
                    case 18:
                        easySkin(R.string.bcg, R.layout.bcgs,"blue_creeper_girl.png");
                        break;
                    case 19:
                        easySkin(R.string.summernight, R.layout.summernights,"summer_night.png");
                        break;
                    case 20:
                        easySkin(R.string.panda, R.layout.pandas,"pink_panda_girl.png");
                        break;
                    case 21:
                        easySkin(R.string.withergirl, R.layout.withergirls,"wither_girl.png");
                        break;
                    case 22:
                        easySkin(R.string.galaxygirl, R.layout.galaxygirls,"galaxy_girl.png");
                        break;
                    case 23:
                        easySkin(R.string.blondegirl, R.layout.blondegirls,"blonde_girl.png");
                        break;
                    case 24:
                        easySkin(R.string.cdg, R.layout.cdgs,"cute_dress_girl.png");
                        break;
                    case 25:
                        easySkin(R.string.candy, R.layout.candys,"candy_pop.png");
                        break;
                    case 26:
                        easySkin(R.string.junglegirl, R.layout.junglegirls,"jungle_girl.png");
                        break;
                    case 27:
                        easySkin(R.string.catgirl, R.layout.catgirls,"cat_girl.png");
                        break;
                    case 28:
                        easySkin(R.string.countrygirl, R.layout.countrygirls,"country_girl.png");
                        break;
                    case 29:
                        easySkin(R.string.cutegirl, R.layout.cutegirls,"cute_girls.png");
                        break;
                    case 30:
                        easySkin(R.string.diamondgirl, R.layout.dgs,"diamond_girl.png");
                        break;
                    case 31:
                        easySkin(R.string.endergirl, R.layout.endergirls,"endergirl.png");
                        break;
                    case 32:
                        easySkin(R.string.rbg, R.layout.rbgs,"rainbow_girl.png");
                        break;
                    case 33:
                        easySkin(R.string.fg, R.layout.fgs,"flower_girl.png");
                        break;
                    case 34:
                        easySkin(R.string.assg, R.layout.assgs,"assasin_girl.png");
                        break;


                        }

                        }
                    /*    if(data.get(position) == (getString(R.string.ocean_queen)))
                        {
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.ocean_queen), (inflater.inflate(R.layout.ocean_queen, null)), "ocean_queen.png"); */
                          /*  new MaterialDialog.Builder(getActivity())
                                    .title(R.string.ocean_queen)
                                    .customView(R.layout.ocean_queen, true)
                                    .positiveText(R.string.close)
                                    .titleColor(getResources().getColor(R.color.dialog))
                                    .positiveColor(getResources().getColor(R.color.dialog))
                                    .negativeColor(getResources().getColor(R.color.dialog))
                                    .negativeText(R.string.applys)
                                    .callback(new MaterialDialog.ButtonCallback() {
                                        @Override
                                        public void onPositive(MaterialDialog dialog) {

                                            dialog.dismiss();
                                        }

                                        @Override
                                        public void onNegative(MaterialDialog dialog) {

                                            CopyFile("ocean_queen.png");
                                        }
                                    }).show(); */
                        /*
                        }

                        if(data.get(position) == (getString(R.string.white_sea_dress)))
                        {
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.white_sea_dress), (inflater.inflate(R.layout.white_sea_dress, null)), "white_sea_dress.png");
                        }

                        if(data.get(position) == (getString(R.string.easter)))
                        {
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.easter), (inflater.inflate(R.layout.easters, null)), "easter.png");

                        }

                        if(data.get(position) == (getString(R.string.fairy)))
                        {
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.fairy), (inflater.inflate(R.layout.fairys, null)), "fairy.png");

                        }
                        if(data.get(position) == (getString(R.string.music_girl)))
                        {
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.music_girl), (inflater.inflate(R.layout.music_girl, null)), "music_girl.png");

                        }
                        if(data.get(position) == (getString(R.string.spring_girl)))
                        {
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.spring_girl), (inflater.inflate(R.layout.spring_girl, null)), "spring_girl.png");

                        }

                        if(data.get(position) == (getString(R.string.tiger_girl)))
                        {
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.tiger_girl), (inflater.inflate(R.layout.tiger_girls, null)), "tiger_girl.png");
                        }

						if (data.get(position) == (getString(R.string.sportsgirl)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.sportsgirl), (inflater.inflate(R.layout.sportgirls, null)), "sports_girl.png");
						}

						if (data.get(position) == (getString(R.string.cakegirl)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.cakegirl), (inflater.inflate(R.layout.cakegirls, null)), "cake_girl.png");
						}

						if (data.get(position) == (getString(R.string.thestar)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.thestar), (inflater.inflate(R.layout.thestars, null)), "the_star.png");
						}

						if (data.get(position) == (getString(R.string.hoodiegirl)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.hoodiegirl), (inflater.inflate(R.layout.hoodiegirls, null)), "hoodie_girl.png");
						}

						if (data.get(position) == (getString(R.string.summertime)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.summertime), (inflater.inflate(R.layout.summertimes, null)), "summer_time.png");
						}

						if (data.get(position) == (getString(R.string.bcg)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.bcg), (inflater.inflate(R.layout.bcgs, null)), "blue_creeper_girl.png");
						}

						if (data.get(position) == (getString(R.string.summernight)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.summernight), (inflater.inflate(R.layout.summernights, null)), "summer_night.png");
						}

						if (data.get(position) == (getString(R.string.panda)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.panda), (inflater.inflate(R.layout.pandas, null)), "pink_panda_girl.png");
						}

						if (data.get(position) == (getString(R.string.withergirl)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.withergirl), (inflater.inflate(R.layout.withergirls, null)), "wither_girl.png");
						}

						if (data.get(position) == (getString(R.string.galaxygirl)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.galaxygirl), (inflater.inflate(R.layout.galaxygirls, null)), "galaxy_girl.png");
						}

						if (data.get(position) == (getString(R.string.blondegirl)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.blondegirl), (inflater.inflate(R.layout.blondegirls, null)), "blonde_girl.png");
						}
						if (data.get(position) == (getString(R.string.cdg)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.cdg), (inflater.inflate(R.layout.cdgs, null)), "cute_dress_girl.png");
						}
						if (data.get(position) == (getString(R.string.candy)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.candy), (inflater.inflate(R.layout.candys, null)), "candy_pop.png");
						}
						if (data.get(position) == (getString(R.string.junglegirl)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.junglegirl), (inflater.inflate(R.layout.junglegirls, null)), "jungle_girl.png");
						}
						if (data.get(position) == (getString(R.string.catgirl)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.catgirl), (inflater.inflate(R.layout.catgirls, null)), "cat_girl.png");
						}
						if (data.get(position) == (getString(R.string.countrygirl)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.countrygirl), (inflater.inflate(R.layout.countrygirls, null)), "country_girl.png");
						}
						if (data.get(position) == (getString(R.string.cutegirl)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.cutegirl), (inflater.inflate(R.layout.cutegirls, null)), "cute_girl.png");
						}
						if (data.get(position) == (getString(R.string.diamondgirl)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.diamondgirl), (inflater.inflate(R.layout.dgs, null)), "diamond_girl.png");
						}
						if (data.get(position) == (getString(R.string.endergirl)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.endergirl), (inflater.inflate(R.layout.endergirls, null)), "endergirl.png");
						}
						if (data.get(position) == (getString(R.string.rbg)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.rbg), (inflater.inflate(R.layout.rbgs, null)), "rainbow_girl.png");
						}
						if (data.get(position) == (getString(R.string.fg)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.fg), (inflater.inflate(R.layout.fgs, null)), "flower_girl.png");
						}
						if (data.get(position) == (getString(R.string.assg)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.assg), (inflater.inflate(R.layout.assgs, null)), "assasin_girl.png");
						}
                */


	});
	return view;
	}


	/** Called before the activity is destroyed. */
	@Override
	public void onDestroy() {
		// Destroy the AdView.

		super.onDestroy();
	}
}
