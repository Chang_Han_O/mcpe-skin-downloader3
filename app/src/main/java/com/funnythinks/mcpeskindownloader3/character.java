package com.funnythinks.mcpeskindownloader3;

import android.annotation.SuppressLint;
import android.app.*;
import android.content.*;
import android.net.*;
import android.os.*;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.*;
import android.widget.*;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.*;

import android.support.v4.app.Fragment;

@SuppressLint("ValidFragment")
public class character extends Fragment
{
	private Context mContext;

    public character(Context context) {
        mContext = context;
    }

    public void DownFile(String filename){
        File new_file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/MCPESKIN/");

        if(!new_file.exists()){
            new_file.mkdirs();
        }
        try {
            InputStream myInput = mContext.getAssets().open(filename);
            String outFileName_ = Environment.getExternalStorageDirectory() + "/MCPESKIN/" + filename;

            OutputStream myOutput_ = new FileOutputStream(outFileName_);
            //  File afile = new File(outFileName_);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput_.write(buffer, 0, length);
            }
            myOutput_.flush();
            myOutput_.close();
            myInput.close();
            Toast.makeText(mContext,getString(R.string.path_download) + outFileName_,Toast.LENGTH_LONG).show();
        }
        catch(IOException e) {
            e.printStackTrace();
            Toast.makeText(mContext,e.toString(),Toast.LENGTH_SHORT).show();
            Toast.makeText(mContext,"ERROR OCCURRED",Toast.LENGTH_SHORT).show();
        }
    }

    public void Apply(String filename){
        File new_file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/games/com.mojang/minecraftpe/");

        if(!new_file.exists()){
            new_file.mkdirs();
        }
        try {
            InputStream myInput = mContext.getAssets().open(filename);
            String outFileName_ = Environment.getExternalStorageDirectory() + "/games/com.mojang/minecraftpe/" + "custom.png";

            OutputStream myOutput_ = new FileOutputStream(outFileName_);
            //  File afile = new File(outFileName_);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput_.write(buffer, 0, length);
            }
            myOutput_.flush();
            myOutput_.close();
            myInput.close();

            new MaterialDialog.Builder(getActivity())
                    .title(getString(R.string.success))
                    .content(getString(R.string.skin_set))
                    .positiveText(R.string.close) //Close String
                    .titleColor(getResources().getColor(R.color.dialog))
                    .positiveColor(getResources().getColor(R.color.dialog))
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {

                            dialog.dismiss();
                        }
                    }).show();
            Toast.makeText(mContext,getString(R.string.success_skin),Toast.LENGTH_SHORT).show();

        }
        catch(IOException e) {
            e.printStackTrace();
            Toast.makeText(mContext,e.toString(),Toast.LENGTH_SHORT).show();
            Toast.makeText(mContext,"ERROR OCCURRED",Toast.LENGTH_SHORT).show();
        }
    }




    public void MaterialDialog(String title, int lay, final String str) {

        new MaterialDialog.Builder(getActivity())
                .title(title)
                .customView(lay, true)
                .positiveText(R.string.close) //Close String
                .titleColor(getResources().getColor(R.color.dialog))
                .positiveColor(getResources().getColor(R.color.dialog))
                .negativeColor(getResources().getColor(R.color.dialog))
                .neutralColor(getResources().getColor(R.color.dialog))
                .negativeText(R.string.applys) //Apply Skin String
                .neutralText(R.string.downskin) //Download String
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                        dialog.dismiss();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {

                        Apply(str);

                    }

                    @Override
                    public void onNeutral(MaterialDialog dialog) {
                        DownFile(str);
                    }
                }).show();

    }

    public void easySkin(int string, int layout, String name) {
        MaterialDialog(getString(string), layout, name);
    }

	ConnectivityManager connMgr;
	private static final String AD_UNIT_ID = "ca-app-pub-1643422267007813/1212287483";
	public final int DIALOG_DOWNLOAD_PROGRESS = 0;
	public ProgressDialog mProgressDialog;/*s	ArrayList<String> data; */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.boy, null);
        connMgr = (ConnectivityManager)
                mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.list);

        ItemData data[] = {

                //v5.5
                new ItemData(getString(R.string.deadpool), R.drawable.deadpoolb),
                new ItemData(getString(R.string.katelyn_the_fire), R.drawable.katelyn_the_fireb),

                //v5.4
                new ItemData(getString(R.string.genos), R.drawable.genosb),
                new ItemData(getString(R.string.saitama), R.drawable.saitamab),
                new ItemData(getString(R.string.three_d_link), R.drawable.three_d_linkb),

                //v5.3
                new ItemData(getString(R.string.ace), R.drawable.aceb),
                new ItemData(getString(R.string.death_eater), R.drawable.death_eaterb),
                new ItemData(getString(R.string.luffy), R.drawable.luffyb),
                new ItemData(getString(R.string.sabo), R.drawable.sabob),
                new ItemData(getString(R.string.zoro), R.drawable.zorob),

                new ItemData(getString(R.string.goku), R.drawable.gokub),
                new ItemData(getString(R.string.mario), R.drawable.mariob),

                new ItemData(getString(R.string.pikachu), R.drawable.pikachub),
                new ItemData(getString(R.string.pvz_zombie), R.drawable.pvz_zombieb),
                new ItemData(getString(R.string.thehunter), R.drawable.thehunterb),
                new ItemData(getString(R.string.dantdm), R.drawable.dantdmb),
                new ItemData(getString(R.string.pooh), R.drawable.poohb),
                new ItemData(getString(R.string.maria), R.drawable.mariab),
                new ItemData(getString(R.string.applejack), R.drawable.applejackb),
                new ItemData(getString(R.string.foxy), R.drawable.foxyb),
                new ItemData(getString(R.string.kirito), R.drawable.kiritob),
                new ItemData(getString(R.string.patrick_star), R.drawable.patrick_starb),
                new ItemData(getString(R.string.spring_trap), R.drawable.spring_trapb),
                new ItemData(getString(R.string.vegetta777), R.drawable.vegetta777b),
                new ItemData(getString(R.string.twilightsparklehuman), R.drawable.twilightsparklehumanb),
                new ItemData(getString(R.string.eren), R.drawable.erenb),
                new ItemData(getString(R.string.mikasa), R.drawable.mikasab),
                new ItemData(getString(R.string.rick), R.drawable.rickb),
                new ItemData(getString(R.string.lforl), R.drawable.lforleeeeexb),
                new ItemData(getString(R.string.stampy), R.drawable.stampylongnoseb),
                new ItemData(getString(R.string.masterchief), R.drawable.masterchiefb),
                new ItemData(getString(R.string.thor), R.drawable.thorb),
                new ItemData(getString(R.string.hulk), R.drawable.hulkb),
                new ItemData(getString(R.string.simpson), R.drawable.simpsonb),
                new ItemData(getString(R.string.iron), R.drawable.ironb),
                new ItemData(getString(R.string.naruto), R.drawable.narutob),
                new ItemData(getString(R.string.elsa), R.drawable.elsab),
                new ItemData(getString(R.string.anna), R.drawable.annab),
                new ItemData(getString(R.string.assc), R.drawable.assasin_creedb),
                new ItemData(getString(R.string.red), R.drawable.tornadob),
                new ItemData(getString(R.string.terminator), R.drawable.terminatorb),
                new ItemData(getString(R.string.captain), R.drawable.captainamericab),
                new ItemData(getString(R.string.miku), R.drawable.mikub),
                new ItemData(getString(R.string.spider), R.drawable.spidermanb),
                new ItemData(getString(R.string.empire), R.drawable.empireb)
        };

        //set layoutManager
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        // 3. create an adapter
        MyAdapter mAdapter = new MyAdapter(data);
        // 4. set adapter
        recyclerView.setAdapter(mAdapter);
        // 5. set item animator to DefaultAnimator
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        mAdapter.SetOnItemClickListener(new MyAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                switch (position) {
                    case 0:
                        easySkin(R.string.deadpool, R.layout.deadpool,"deadpool.png");
                        break;
                    case 1:
                        easySkin(R.string.katelyn_the_fire, R.layout.katelyn_the_fire,"katelyn_the_fire.png");
                        break;
                    case 2:
                        easySkin(R.string.genos, R.layout.genos,"genos.png");
                        break;
                    case 3:
                        easySkin(R.string.saitama, R.layout.saitama,"saitama.png");
                        break;
                    case 4:
                        easySkin(R.string.three_d_link, R.layout.three_d_link,"three_d_link.png");
                        break;
                    case 5:
                        easySkin(R.string.ace, R.layout.ace,"ace.png");
                        break;
                    case 6:
                        easySkin(R.string.death_eater, R.layout.death_eater,"death_eater.png");
                        break;
                    case 7:
                        easySkin(R.string.luffy, R.layout.luffy,"luffy.png");
                        break;
                    case 8:
                        easySkin(R.string.sabo, R.layout.sabo,"sabo.png");
                        break;
                    case 9:
                        easySkin(R.string.zoro, R.layout.zoro,"zoro.png");
                        break;
                    case 10:
                        easySkin(R.string.goku, R.layout.goku,"goku.png");
                        break;
                    case 11:
                        easySkin(R.string.mario, R.layout.mario,"mario.png");
                        break;
                    case 12:
                        easySkin(R.string.pikachu, R.layout.pikachu,"pikachu.png");
                        break;
                    case 13:
                        easySkin(R.string.pvz_zombie, R.layout.pvz_zombie,"pvz_zombie.png");
                        break;
                    case 14:
                        easySkin(R.string.thehunter, R.layout.thehunter,"thehunter.png");
                        break;
                    case 15:
                        easySkin(R.string.dantdm, R.layout.dantdm,"dantdm.png");
                        break;
                    case 16:
                        easySkin(R.string.pooh, R.layout.pooh,"pooh.png");
                        break;
                    case 17:
                        easySkin(R.string.maria, R.layout.maria, "maria.png");
                        break;
                    case 18:
                        easySkin(R.string.applejack, R.layout.applejacks,"applejack.png");
                        break;
                    case 19:
                        easySkin(R.string.foxy, R.layout.foxys,"foxy.png");
                        break;
                    case 20:
                        easySkin(R.string.kirito, R.layout.kiritos,"kirito.png");
                        break;
                    case 21:
                        easySkin(R.string.patrick_star, R.layout.patrick_stars,"patrick_star.png");
                        break;
                    case 22:
                        easySkin(R.string.spring_trap, R.layout.spring_traps,"spring_trap.png");
                        break;
                    case 23:
                        easySkin(R.string.vegetta777, R.layout.vegetta777s,"vegetta777.png");
                        break;
                    case 24:
                        easySkin(R.string.twilightsparklehuman, R.layout.twilightsparklehumans,"twilight_sparkle_human.png");
                        break;
                    case 25:
                        easySkin(R.string.eren, R.layout.erens,"eren.png");
                        break;
                    case 26:
                        easySkin(R.string.mikasa, R.layout.mikasas,"mikasa.png");
                        break;
                    case 27:
                        easySkin(R.string.rick, R.layout.ricks,"rick.png");
                        break;
                    case 28:
                        easySkin(R.string.lforl, R.layout.lforls,"l_for_leeeee_x.png");
                        break;
                    case 29:
                        easySkin(R.string.stampy, R.layout.stampys,"stampylongnose.png");
                        break;
                    case 30:
                        easySkin(R.string.masterchief, R.layout.masterchiefs,"master_chief.png");
                        break;
                    case 31:
                        easySkin(R.string.thor, R.layout.thors,"thor.png");
                        break;
                    case 32:
                        easySkin(R.string.hulk, R.layout.hulks,"hulk.png");
                        break;
                    case 33:
                        easySkin(R.string.simpson, R.layout.simpsons,"simpson.png");
                        break;
                    case 34:
                        easySkin(R.string.iron, R.layout.irons,"iron_man.png");
                        break;
                    case 35:
                        easySkin(R.string.naruto, R.layout.narutos,"naruto.png");
                        break;
                    case 36:
                        easySkin(R.string.elsa, R.layout.elsas,"elsa.png");
                        break;
                    case 37:
                        easySkin(R.string.anna, R.layout.annas,"anna.png");
                        break;
                    case 38:
                        easySkin(R.string.assc, R.layout.asscs,"assasin_creed.png");
                        break;
                    case 39:
                        easySkin(R.string.red, R.layout.red,"red_tornado.png");
                        break;
                    case 40:
                        easySkin(R.string.terminator, R.layout.terminator,"terminator.png");
                        break;
                    case 41:
                        easySkin(R.string.captain, R.layout.captain,"captain_america.png");
                        break;
                    case 42:
                        easySkin(R.string.miku, R.layout.mikus,"miku.png");
                        break;
                    case 43:
                        easySkin(R.string.spider, R.layout.spiders,"spiderman.png");
                        break;
                    case 44:
                        easySkin(R.string.empire, R.layout.empires,"empire.png");
                        break;
                }
            }

/*
		data = new ArrayList<String>();

        //v2.6 skin
        data.add(getString(R.string.maria));

        //v2.0 skins msd3
        data.add(getString(R.string.applejack));
        data.add(getString(R.string.foxy));
        data.add(getString(R.string.kirito));
        data.add(getString(R.string.patrick_star));
        data.add(getString(R.string.spring_trap));
		
		data.add(getString(R.string.vegetta777));
		data.add(getString(R.string.twilightsparklehuman));
		data.add(getString(R.string.eren));
		data.add(getString(R.string.mikasa));
		data.add(getString(R.string.rick));
		
		data.add(getString(R.string.lforl));
		data.add(getString(R.string.stampy));
		// v2.1 skin
		data.add(getString(R.string.masterchief));
		data.add(getString(R.string.thor));
		data.add(getString(R.string.hulk));
		data.add(getString(R.string.simpson));
		data.add(getString(R.string.iron));
		data.add(getString(R.string.naruto));
		data.add(getString(R.string.elsa));
		data.add(getString(R.string.anna));
		data.add(getString(R.string.assc));
		data.add(getString(R.string.red));
		data.add(getString(R.string.terminator));

		data.add(getString(R.string.captain));
		data.add(getString(R.string.miku));
		data.add(getString(R.string.spider));
		data.add(getString(R.string.empire));

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
			getActivity(), R.layout.simple_list_item_1, data);

		ListView list = (ListView)view.findViewById(R.id.list);
		list.setAdapter(adapter);

		list.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					data.get(position);
					{
                        if (data.get(position) == (getString(R.string.maria)))
                        {
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.maria), (inflater.inflate(R.layout.maria, null)), "maria.png");
                        }
                        if (data.get(position) == (getString(R.string.applejack)))
                        {
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.applejack), (inflater.inflate(R.layout.applejacks, null)), "applejack.png");
                        }
                        if (data.get(position) == (getString(R.string.foxy)))
                        {
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.foxy), (inflater.inflate(R.layout.foxys, null)), "foxy.png");
                        }
                        if (data.get(position) == (getString(R.string.kirito)))
                        {
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.kirito), (inflater.inflate(R.layout.kiritos, null)), "kirito.png");
                        }
                        if (data.get(position) == (getString(R.string.patrick_star)))
                        {
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.patrick_star), (inflater.inflate(R.layout.patrick_stars, null)), "patrick_star.png");
                        }
                        if (data.get(position) == (getString(R.string.spring_trap)))
                        {
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.spring_trap), (inflater.inflate(R.layout.spring_traps, null)), "spring_trap.png");
                        }
						if (data.get(position) == (getString(R.string.vegetta777)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.vegetta777), (inflater.inflate(R.layout.vegetta777s, null)), "vegetta777.png");
						}
						if (data.get(position) == (getString(R.string.twilightsparklehuman)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.twilightsparklehuman), (inflater.inflate(R.layout.twilightsparklehumans, null)), "twilight_sparkle_human.png");
						}
						if (data.get(position) == (getString(R.string.eren)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.eren), (inflater.inflate(R.layout.erens, null)), "eren.png");
						}
						if (data.get(position) == (getString(R.string.mikasa)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.mikasa), (inflater.inflate(R.layout.mikasas, null)), "mikasa.png");
						}
						if (data.get(position) == (getString(R.string.rick)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.rick), (inflater.inflate(R.layout.ricks, null)), "rick.png");
						}
						if (data.get(position) == (getString(R.string.lforl)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.lforl), (inflater.inflate(R.layout.lforls, null)), "l_for_leeeee_x.png");
						}
						if (data.get(position) == (getString(R.string.stampy)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.stampy), (inflater.inflate(R.layout.stampys, null)), "stampylongnose.png");
						}
						if (data.get(position) == (getString(R.string.masterchief)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.masterchief), (inflater.inflate(R.layout.masterchiefs, null)), "master_chief.png");
						}
						if (data.get(position) == (getString(R.string.thor)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.thor), (inflater.inflate(R.layout.thors, null)), "thor.png");
						}
						if (data.get(position) == (getString(R.string.hulk)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.hulk), (inflater.inflate(R.layout.hulks, null)), "hulk.png");
						}
						if (data.get(position) == (getString(R.string.simpson)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.simpson), (inflater.inflate(R.layout.simpsons, null)), "simpson.png");
						}
						if (data.get(position) == (getString(R.string.iron)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.iron), (inflater.inflate(R.layout.irons, null)), "iron_man.png");
						}
						if (data.get(position) == (getString(R.string.naruto)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.naruto), (inflater.inflate(R.layout.narutos, null)), "naruto.png");
						}
						if (data.get(position) == (getString(R.string.elsa)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.elsa), (inflater.inflate(R.layout.elsas, null)), "elsa.png");
						}
						if (data.get(position) == (getString(R.string.anna)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.anna), (inflater.inflate(R.layout.annas, null)), "anna.png");
						}
						if (data.get(position) == (getString(R.string.assc)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.assc), (inflater.inflate(R.layout.asscs, null)), "assasin_creed.png");
						}
						if (data.get(position) == (getString(R.string.red)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.red), (inflater.inflate(R.layout.red, null)), "red_tornado.png");
						}
						if (data.get(position) == (getString(R.string.terminator)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.terminator), (inflater.inflate(R.layout.terminator, null)), "terminator.png");
						}
						if (data.get(position) == (getString(R.string.captain)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.captain), (inflater.inflate(R.layout.captain, null)), "captain_america.png");
						}
						if (data.get(position) == (getString(R.string.empire)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.empire), (inflater.inflate(R.layout.empires, null)), "empire.png");
						}
						if (data.get(position) == (getString(R.string.spider)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.spider), (inflater.inflate(R.layout.spiders, null)), "spiderman.png");
						}
						if (data.get(position) == (getString(R.string.miku)))
						{
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            MaterialDialog(getString(R.string.miku), (inflater.inflate(R.layout.mikus, null)), "miku.png");
						}
							*/
        });

			return view;
			}

	/** Called before the activity is destroyed. */
	@Override
	public void onDestroy() {
		// Destroy the AdView.

		super.onDestroy();
	}
}
