package com.funnythinks.mcpeskindownloader3;

import android.graphics.Color;
import android.os.*;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.*;
import android.widget.*;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class blocksave extends ActionBarActivity {
    private ViewGroup rowContainer;
    private View view;
    private AdView adView;
    private static final String AD_UNIT_ID = "ca-app-pub-1643422267007813/1212287483";

    public void fillRow(View view, final String title) {

        TextView textView1 = (TextView) view.findViewById(R.id.textview_blocksave);
        textView1.setText(title);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blocksave);

        adView = new AdView(this);
        adView.setAdSize(AdSize.SMART_BANNER);
        adView.setAdUnitId(AD_UNIT_ID);

        RelativeLayout layout = (RelativeLayout) findViewById(R.id.adlayout);
        layout.addView(adView);

        AdRequest adRequest = new AdRequest.Builder()
                .build();

        adView.loadAd(adRequest);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setTitleTextColor(Color.WHITE);

        ActionBar mActionBar = getSupportActionBar();

        if (mActionBar != null) {
            mActionBar.setHomeButtonEnabled(true);
            mActionBar.setDisplayHomeAsUpEnabled(true);

            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        rowContainer = (LinearLayout) findViewById(R.id.rowContainer_blocksave);


        view = rowContainer.findViewById(R.id.card1_blocksave);
        fillRow(view, getString(R.string.apply_one));
        ((ImageView) view.findViewById(R.id.imageview_blocksave)).setImageResource(R.drawable.apply_);

        view = rowContainer.findViewById(R.id.card2_blocksave);
        fillRow(view, getString(R.string.apply_two));
        ((ImageView) view.findViewById(R.id.imageview_blocksave)).setImageResource(R.drawable.apply_two);

    }

    public void onResume() {
        super.onResume();
        if (adView != null) {
            adView.resume();
        }
    }

    @Override
    public void onPause() {
        if (adView != null) {
            adView.pause();
        }
        super.onPause();
    }

    /** Called before the activity is destroyed. */
    @Override
    public void onDestroy() {
        // Destroy the AdView.
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }
}
