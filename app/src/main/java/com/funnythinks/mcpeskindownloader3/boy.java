package com.funnythinks.mcpeskindownloader3;

import android.annotation.SuppressLint;
import android.app.*;
import android.content.*;
import android.net.*;
import android.os.*;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.*;
import android.widget.*;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.*;

import android.support.v4.app.Fragment;

@SuppressLint("ValidFragment")
public class boy extends Fragment {
	
	private Context mContext;

    public boy(Context context) {
        mContext = context;
    }

	ConnectivityManager connMgr;
	private static final String AD_UNIT_ID = "ca-app-pub-1643422267007813/1212287483";
	public final int DIALOG_DOWNLOAD_PROGRESS = 0;
	public ProgressDialog mProgressDialog;


public void DownFile(String filename){
    File new_file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/MCPESKIN/");

    if(!new_file.exists()){
        new_file.mkdirs();
    }
    try {
        InputStream myInput = mContext.getAssets().open(filename);
        String outFileName_ = Environment.getExternalStorageDirectory() + "/MCPESKIN/" + filename;

        OutputStream myOutput_ = new FileOutputStream(outFileName_);
        //  File afile = new File(outFileName_);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput_.write(buffer, 0, length);
        }
        myOutput_.flush();
        myOutput_.close();
        myInput.close();
        Toast.makeText(mContext,getString(R.string.path_download) + outFileName_,Toast.LENGTH_LONG).show();
    }
    catch(IOException e) {
        e.printStackTrace();
        Toast.makeText(mContext,e.toString(),Toast.LENGTH_SHORT).show();
        Toast.makeText(mContext,"ERROR OCCURRED",Toast.LENGTH_SHORT).show();
    }
}

    public void Apply(String filename){
        File new_file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/games/com.mojang/minecraftpe/");

        if(!new_file.exists()){
            new_file.mkdirs();
        }
        try {
            InputStream myInput = mContext.getAssets().open(filename);
            String outFileName_ = Environment.getExternalStorageDirectory() + "/games/com.mojang/minecraftpe/" + "custom.png";

            OutputStream myOutput_ = new FileOutputStream(outFileName_);
            //  File afile = new File(outFileName_);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput_.write(buffer, 0, length);
            }
            myOutput_.flush();
            myOutput_.close();
            myInput.close();

            new MaterialDialog.Builder(getActivity())
                    .title(getString(R.string.success))
                    .content(getString(R.string.skin_set))
                    .positiveText(R.string.close) //Close String
                    .titleColor(getResources().getColor(R.color.dialog))
                    .positiveColor(getResources().getColor(R.color.dialog))
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {

                            dialog.dismiss();
                        }
                    }).show();
            Toast.makeText(mContext,getString(R.string.success_skin),Toast.LENGTH_SHORT).show();

        }
        catch(IOException e) {
            e.printStackTrace();
            Toast.makeText(mContext,e.toString(),Toast.LENGTH_SHORT).show();
            Toast.makeText(mContext,"ERROR OCCURRED",Toast.LENGTH_SHORT).show();
        }
    }




    public void MaterialDialog(String title, int lay, final String str) {

        new MaterialDialog.Builder(getActivity())
                .title(title)
                .customView(lay, true)
                .positiveText(R.string.close) //Close String
                .titleColor(getResources().getColor(R.color.dialog))
                .positiveColor(getResources().getColor(R.color.dialog))
                .negativeColor(getResources().getColor(R.color.dialog))
                .neutralColor(getResources().getColor(R.color.dialog))
                .negativeText(R.string.applys) //Apply Skin String
                .neutralText(R.string.downskin) //Download String
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {

                        dialog.dismiss();
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {

                        Apply(str);

                    }

                    @Override
                    public void onNeutral(MaterialDialog dialog) {
                        DownFile(str);
                    }
                }).show();

    }


    public void easySkin(int string, int layout, String name) {
        MaterialDialog(getString(string), layout, name);
    }
	
@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { //Fragment OnCreateView
	View view = inflater.inflate(R.layout.boy, null); //View Inflate
	connMgr = (ConnectivityManager)
		mContext.getSystemService(Context.CONNECTIVITY_SERVICE); //Network Check

    RecyclerView recyclerView = (RecyclerView)view.findViewById(R.id.list);

	ItemData data[] = {

            //v5.2 skin
            new ItemData(getString(R.string.santa_claus), R.drawable.santa_clausb),

            //v2.6 skin
            new ItemData(getString(R.string.sword_boy), R.drawable.sword_boyb),

            //v2.1 skin
            new ItemData(getString(R.string.blue_boy), R.drawable.blue_boyb),
    new ItemData(getString(R.string.drop_the_bass), R.drawable.drop_the_bassb),
    new ItemData(getString(R.string.eletric_boy), R.drawable.eletric_boyb),
    new ItemData(getString(R.string.flame_boy), R.drawable.flame_boyb),
    new ItemData(getString(R.string.green_teen_boy), R.drawable.green_teen_boyb),
    new ItemData(getString(R.string.enderboy), R.drawable.enderboyb),
    new ItemData(getString(R.string.cookieboy), R.drawable.cookieboyb),
    new ItemData(getString(R.string.redyoutuber), R.drawable.redyoutuberb),
    new ItemData(getString(R.string.fireboy), R.drawable.fireboyb),
    new ItemData(getString(R.string.foxboy), R.drawable.foxboyb),

    new ItemData(getString(R.string.tnt), R.drawable.tntgrieferb),
    new ItemData(getString(R.string.witherboy), R.drawable.witherboyb),
    new ItemData(getString(R.string.stupid), R.drawable.stupidb),
    new ItemData(getString(R.string.hunter), R.drawable.hunterb),
    new ItemData(getString(R.string.alex), R.drawable.alexb),
    new ItemData(getString(R.string.suitb), R.drawable.bluehairb),
    new ItemData(getString(R.string.coon), R.drawable.coonb),
    new ItemData(getString(R.string.rainbow), R.drawable.rainbowb),
    new ItemData(getString(R.string.psy), R.drawable.psyb),
    new ItemData(getString(R.string.godon), R.drawable.godonb),
    new ItemData(getString(R.string.soldier), R.drawable.soldierb),
    new ItemData(getString(R.string.noname1), R.drawable.justb),
    new ItemData(getString(R.string.noname2), R.drawable.just2b),
    new ItemData(getString(R.string.psysiel), R.drawable.psy_sielb),
    new ItemData(getString(R.string.bbabak), R.drawable.bbabakb),
    new ItemData(getString(R.string.king), R.drawable.kingb),
    new ItemData(getString(R.string.taekwondo), R.drawable.taekwondob),
    new ItemData(getString(R.string.agent), R.drawable.masterb),
    new ItemData(getString(R.string.cute), R.drawable.cuteboyb),
    new ItemData(getString(R.string.freeboy), R.drawable.freeboyb),
    new ItemData(getString(R.string.leez), R.drawable.leezb),
    new ItemData(getString(R.string.jsh), R.drawable.highb),
    new ItemData(getString(R.string.guardian), R.drawable.skillb),
    new ItemData(getString(R.string.notch), R.drawable.notchb)
    };


    // 2. set layoutManger
    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    // 3. create an adapter
    MyAdapter mAdapter = new MyAdapter(data);
    // 4. set adapter
    recyclerView.setAdapter(mAdapter);
    // 5. set item animator to DefaultAnimator
    recyclerView.setItemAnimator(new DefaultItemAnimator());

	mAdapter.SetOnItemClickListener(new MyAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(View v, int position) {
            switch (position) {
                case 0:
                    easySkin(R.string.santa_claus, R.layout.santa_claus,"santa_claus.png");
                    break;
                case 1:
                    easySkin(R.string.sword_boy, R.layout.sword_boy,"sword_boy.png");
                    break;
                case 2:
                    easySkin(R.string.blue_boy, R.layout.blue_boys, "blue_boy.png");
                    break;
                case 3:
                    easySkin(R.string.drop_the_bass, R.layout.drop_the_basss,"drop_the_bass.png");
                    break;
                case 4:
                    easySkin(R.string.eletric_boy, R.layout.eletric_boys,"eletric_boy.png");
                    break;
                case 5:
                    easySkin(R.string.flame_boy, R.layout.flame_boys,"flame_boy.png");
                    break;
                case 6:
                    easySkin(R.string.green_teen_boy, R.layout.green_teen_boys,"green_teen_boy.png");
                    break;
                case 7:
                    easySkin(R.string.enderboy, R.layout.enderboys,"ender_boy.png");
                    break;
                case 8:
                    easySkin(R.string.cookieboy, R.layout.cookieboys,"cookie_boy.png");
                    break;
                case 9:
                    easySkin(R.string.redyoutuber, R.layout.redyoutubers,"red_youtuber.png");
                    break;
                case 10:
                    easySkin(R.string.fireboy, R.layout.fireboys,"fire_boy.png");
                    break;
                case 11:
                    easySkin(R.string.foxboy, R.layout.foxboys,"fox_boy.png");
                    break;
                case 12:
                    easySkin(R.string.tnt, R.layout.tnts,"tnt_griefer.png");
                    break;
                case 13:
                    easySkin(R.string.witherboy, R.layout.witherboys,"wither_boy.png");
                    break;
                case 14:
                    easySkin(R.string.stupid, R.layout.stupids,"stupid.png");
                    break;
                case 15:
                    easySkin(R.string.hunter, R.layout.hunters,"hunter.png");
                    break;
                case 16:
                    easySkin(R.string.alex, R.layout.alexs,"alex.png");
                    break;
                case 17:
                    easySkin(R.string.suitb, R.layout.suitbs,"blue_hair_suit.png");
                    break;
                case 18:
                    easySkin(R.string.coon, R.layout.coons,"coon.png");
                    break;
                case 19:
                    easySkin(R.string.rainbow, R.layout.rainbow,"rainbowman.png");
                    break;
                case 20:
                    easySkin(R.string.psy, R.layout.psy,"psy_internet.png");
                    break;
                case 21:
                    easySkin(R.string.godon, R.layout.godon,"godon.png");
                    break;
                case 22:
                    easySkin(R.string.soldier, R.layout.soldier,"soldier.png");
                    break;
                case 23:
                    easySkin(R.string.noname1, R.layout.noname1,"no_have_name1.png");
                    break;
                case 24:
                    easySkin(R.string.noname2, R.layout.noname2,"no_have_name2.png");
                    break;
                case 25:
                    easySkin(R.string.psysiel, R.layout.psysiel,"psy_siel.png");
                    break;
                case 26:
                    easySkin(R.string.bbabak, R.layout.bbabak,"bbabak.png");
                    break;
                case 27:
                    easySkin(R.string.king, R.layout.king,"king.png");
                    break;
                case 28:
                    easySkin(R.string.taekwondo, R.layout.taekwondo,"taekwondo.png");
                    break;
                case 29:
                    easySkin(R.string.agent, R.layout.agent,"agent.png");
                    break;
                case 30:
                    easySkin(R.string.cute, R.layout.cute,"cute.png");
                    break;
                case 31:
                    easySkin(R.string.freeboy, R.layout.freeboy,"freeboy.png");
                    break;
                case 32:
                    easySkin(R.string.leez, R.layout.leez,"leez.png");
                    break;
                case 33:
                    easySkin(R.string.jsh, R.layout.jsh,"highschool_student.png");
                    break;
                case 34:
                    easySkin(R.string.guardian, R.layout.guardian,"guardian.png");
                    break;
                case 35:
                    easySkin(R.string.notch, R.layout.notch,"notch.png");
                    break;

            }

        }
    });
	return view;
}

	/** Called before the activity is destroyed. */
	@Override
	public void onDestroy() {
		// Destroy the AdView.

		super.onDestroy();
	}
}
