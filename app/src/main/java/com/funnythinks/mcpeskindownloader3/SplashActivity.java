package com.funnythinks.mcpeskindownloader3;

import android.app.*;
import android.content.*;
import android.net.*;
import android.os.*;
import android.view.*;
import android.widget.*;

public class SplashActivity extends Activity
{
	ConnectivityManager connMgr;
    @Override
    public void onCreate(Bundle savedInstanceState) {

		connMgr = (ConnectivityManager)
			getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {
			Handler han = new Handler();
			han.postDelayed(new Runnable (){
					@Override
					public void run(){
						startActivity(new Intent(SplashActivity.this,MainActivity.class));
						finish();
					}

				},600);
		}
		else {
			Toast.makeText(getApplicationContext(),
						   R.string.plzchk, Toast.LENGTH_SHORT).show();
			finish();
		}

		super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
		}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}
	
}
 
