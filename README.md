# **MCPE Skin Downloader3** #

MCPE Skin Downloader3의 소스 코드 입니다.

MCPE Skin Downloader3는
스마트폰 게임 마인크래프트 포켓에디션(Minecraft Pocket-Edition)을 할 때
스킨(외형)을 보다 쉽고 빠르고, 간편하게 적용해주는 앱입니다.









# **ScreenShots** #
![image1.png](image/image1.png)

![image2.png](image/image2.png)

![image3.png](image/image3.png)





# **참고, 활용, 사용한 라이브러리** #


** Android PagerSlidingTabStrip Library **

https://github.com/astuetz/PagerSlidingTabStrip

** MaterialDesign Library **

https://github.com/navasmdc/MaterialDesignLibrary

** Material Dialogs Library **

https://github.com/afollestad/material-dialogs

** DrawerArrowDrawable **

https://github.com/ChrisRenke/DrawerArrowDrawable